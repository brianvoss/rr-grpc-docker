# Generate a cert #

cd php-grpc-server && docker run -t -i -v "`pwd`:/go-cert-gen" golang:1.11 /bin/bash

- Inside Container:
cd /go-cert-gen/certs && go run generate_cert.go -host "rr-grpc" -ecdsa-curve "P256"
exit

You should now see these files in /certs
server.crt
server.key

# Start the service #
docker-compose up -d

# Run a client #
docker build -t grpc_client containers/rr-grpc-client/
docker run -t -i -v "`pwd`:/app" \
  --network=bridge \
  --link=rr-grpc \
  grpc_client:latest \
  go run /app/client/main.go "a lower case string"